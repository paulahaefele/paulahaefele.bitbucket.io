::mod:`optionalgo` -- Options Algorithmic Logic
==============================================================

.. module::optionalgo
    :synopsis: Classes for managing option based algorithms

Overview
---------
The options algo module allows for the easy creation of algorithms that trade Options as well as their underlying. It is optimized to use market data provided by OptionMetrics. There are two main classes:

    * :py:class:`optionalgo.OptionAlgo`: Container of algorithmic logic to trade equity Options. Contains a list of OptionsTraders.
    * :py:class:`optionalgo.OptionTrader`: Handles the trading of a specific contract through time. It will hold only a single option contract through time, but handles various aspects to trading tht contract.

.. moduleauthor:: Paul Haefele

Public Methods
--------------

.. automodule:: optionalgo
    :noindex:

Private Methods
---------------

.. automodule:: optionalgo
    :private-members:

::mod:`backtest` -- Backtest Framework
==============================================================

.. module::backtest
    :synopsis: Framework to start algo code, present it with market data while moving clock forward as needed.

Overview
---------

The backtest module allows an algorithm to be run in *sped-up* time. It performs the following functions:

    * Initially loads up the market data required for the whole start to end period.
    * Iterates through the market data bar-by-bar, performing the following actions:
       * Starts the algo (via :py:meth:`algo.Algo.start`) for each new day, and stops the algo (via :py:meth:`algo.Algo.stop` at the end of each day
       * Runs the clock for any scheduled items that need to run
       * Updates the bid/ask data for the algorithm to make decisions.
       * Uses the open/high/low/close data to determine whether an algo should fill or not
       * Rolls positions as required, including EOD positions
    * The algorithm is not shown market data that is out of line with the clock, and the simulated market sees any data first. Thus
      it is not possible to write an algorithm that has future knowledge without going to lengths to circumvent Irwin (which we assume a developer has no interest in doing.)


.. moduleauthor:: Paul Haefele

Public Methods
--------------

.. automodule:: backtest
    :noindex:

Private Methods
---------------

.. automodule:: backtest
    :private-members:

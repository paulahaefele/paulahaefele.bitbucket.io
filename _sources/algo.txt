::mod:`algo` -- Module to create and manage algorithmic logic
==============================================================

.. module::algo
    :synopsis: Classes to assist in creating Algorithms in Irwin

Overview
---------
The algo module is the primary package for creating algorithms. The important classes in this module are:

    * :py:class:`algo.Algo`: Users derive from the Algo class to implemented their logic. They override the :py:meth:`algo.Algo.start` method and from there
      they schedule events either via:

        * :py:class:`utils.Clock` for timer based events, or
        * :py:class:`trade.Order` for market order based events (e.g. fills, cancels)

    * :py:class:`algo.AlgoConfig`: As each algo can run with different *magic numbers* or other configuration, this is supplied externally
      to the Algo logic.
    * :py:func:`algo.backtest_algo` function is provided to run one/many AlgoConfigs against a single algo, recording the results
    * :py:class:`algo.ExecutionTactics` are the base class for order placement logic that required complex behavior. Typically algos
      want to do complex order manipulation, and ExecutionTactics help implemented such behavior.
    * :py:class:`algo.AlgoData` / ccm_algo and :py:class:`algo.RunData` / ccm_run are model classes to hold data relating to the run in the DB.
    * :py:class:`algo.OptionAlgo` is a class derived from algo.Algo that simplifies the running of algos targetting Options trading.

.. moduleauthor:: Paul Haefele

Simple Web-based Examples
---------

The following source code creates two simple Option trading algorithms. These can be deployed as a web page, and the
parameters and their documentation are detailed in the code below. The algorithms defined below can also be run
programmatically.

The are two algorithms:

1.  First sells calls on some percentage of the portfolio value. The strike on the calls sold is
    configurable via the call_delta parameter.
2. Second algo buys straddles when a signal is below a certain level.

.. literalinclude:: ../../algos/web.py


Public Methods
--------------

.. automodule:: algo
    :noindex:

Private Methods
---------------

.. automodule:: algo
    :private-members:

::mod:`security` -- Models tradable securities
==============================================================

.. module::security
    :synopsis: Models the various securities (equity, equity option, FX, continuous futures) that Irwin can trade.

Overview
---------

Models the various securities that Irwin can trade. The :py:class:`security.SecurityManager` manages the loading and caching of securities.

.. moduleauthor:: Paul Haefele

Public Methods
--------------

.. automodule:: security
    :noindex:

Private Methods
---------------

.. automodule:: security
    :private-members:
